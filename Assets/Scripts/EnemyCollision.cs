using UnityEngine;

public class EnemyCollision : MonoBehaviour
{
    public float damageAmount = 10f; // Cantidad de da�o a aplicar a la barra de vida

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            HealthBar healthBar = FindObjectOfType<HealthBar>();
            if (healthBar != null)
            {
                healthBar.DecreaseHealth(damageAmount);
            }
        }
    }
}
