using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    // Referencia al script HealthBar
    public HealthBar healthBar;

    void Start()
    {
        healthBar = FindObjectOfType<HealthBar>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Enemy"))
        {
            healthBar.DecreaseHealth(10f); // Aplicar da�o a la barra de vida
        }
    }
}

