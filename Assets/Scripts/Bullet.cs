using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 10f; // Velocidad del proyectil en unidades por segundo (puedes ajustar este valor seg�n tus necesidades)
    public float lifetime = 3f; // Tiempo de vida del proyectil en segundos

    void Start()
    {
        // Aplicar una velocidad inicial al proyectil hacia adelante (en la direcci�n del eje Z local)
        GetComponent<Rigidbody>().velocity = transform.forward * speed;

        // Destruir el proyectil despu�s de cierto tiempo
        Destroy(gameObject, lifetime);
    }

    void Update()
    {
        // Puedes agregar l�gica adicional aqu� si deseas hacer algo mientras el proyectil est� en movimiento.
    }
}

