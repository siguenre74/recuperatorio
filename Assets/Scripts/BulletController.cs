using UnityEngine;

public class BulletController : MonoBehaviour
{
    public void OnCollisionEnter(Collision collision)
    {
        // Verificar si el objeto colisionado tiene el tag "Enemy"
        if (collision.gameObject.CompareTag("Enemy"))
        {
            // Destruir tanto el objeto del enemigo como el proyectil
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
    }
}
