using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] enemyPrefabs; // Lista de prefabs que deseas instanciar
    public Transform spawnPoint;      // Punto donde deseas que aparezcan los prefabs
    public float spawnInterval = 3f;  // Intervalo de tiempo en segundos entre las apariciones

    void Start()
    {
        // Iniciar la invocación repetitiva de la función SpawnEnemy
        InvokeRepeating("SpawnRandomEnemy", 0f, spawnInterval);
    }

    void SpawnRandomEnemy()
    {
        // Elegir aleatoriamente un prefab de la lista
        int randomIndex = Random.Range(0, enemyPrefabs.Length);
        GameObject randomEnemyPrefab = enemyPrefabs[randomIndex];

        // Instanciar el prefab en la posición del spawnPoint
        Instantiate(randomEnemyPrefab, spawnPoint.position, spawnPoint.rotation);
    }
}
