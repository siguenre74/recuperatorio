using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider; // Referencia al slider de la barra de vida
    public float startingHealth = 100f; // Valor inicial de la barra de vida

    private float currentHealth; // Vida actual
    private bool gameOver = false; // Variable para controlar si el juego ha terminado

    void Start()
    {
        slider = GetComponent<Slider>();
        currentHealth = startingHealth;
        UpdateHealthBar();
    }

    void UpdateHealthBar()
    {
        slider.value = currentHealth; // Actualizar el valor del slider

        // Verificar si la barra de vida ha llegado a cero
        if (currentHealth <= 0 && !gameOver)
        {
            gameOver = true;
            GameOver();
        }
    }

    public void DecreaseHealth(float amount)
    {
        currentHealth -= amount; // Disminuir la vida
        UpdateHealthBar(); // Actualizar la barra de vida
    }

    // En el m�todo GameOver del script HealthBar
    void GameOver()
    {
        Debug.Log("Perdiste"); // Mostrar mensaje de "Perdiste"
        Time.timeScale = 0f; // Congelar el juego
    }

}

