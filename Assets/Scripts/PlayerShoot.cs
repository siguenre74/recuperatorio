using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    public Transform firePoint; // Punto desde donde se dispara
    public GameObject bulletPrefab; // Prefab del proyectil
    public float fireRate = 0.5f; // Velocidad de disparo en segundos
    private float nextFireTime = 0f; // Tiempo en que se permitir� el pr�ximo disparo
    private AudioSource shootingAudio; // Referencia al AudioSource para reproducir el sonido

    void Start()
    {
        shootingAudio = GetComponent<AudioSource>();
    }

    void Update()
    {
        // Disparar al presionar la tecla de disparo (por ejemplo, "W" o "Espacio")
        if (Input.GetKeyDown(KeyCode.W) && Time.time >= nextFireTime)
        {
            Shoot();
            nextFireTime = Time.time + 1f / fireRate;

            // Reproducir el sonido de disparo
            if (shootingAudio != null)
            {
                shootingAudio.Play();
            }
        }
    }

    void Shoot()
    {
        // L�gica para instanciar y disparar el proyectil
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    }
}

