using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float movementSpeed = 5f; // Velocidad del enemigo en unidades por segundo (puedes ajustar este valor seg�n tus necesidades)

    void Update()
    {
        // Mover el enemigo hacia adelante en l�nea recta
        transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);

        // Invocar la funci�n para destruir despu�s de 8 segundos
        Invoke("DestroyObject", 8f);
    }

    void DestroyObject()
    {
        Destroy(gameObject);
    }
}
