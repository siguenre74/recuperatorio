using UnityEngine;

public class DestroyAfterDelay : MonoBehaviour
{
    public float delay = 3f; // Tiempo en segundos antes de destruir el objeto

    void Start()
    {
        // Destruir el objeto despu�s del tiempo especificado
        Destroy(gameObject, delay);
    }
}
