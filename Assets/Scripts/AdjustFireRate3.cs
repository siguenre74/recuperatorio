using UnityEngine;

public class AdjustFireRate3 : MonoBehaviour
{
    public string targetTag = "Player"; // Tag del objeto con el que colisionar para ajustar el Fire Rate

    private float destroyTime = 3f; // Tiempo en segundos antes de destruir el objeto

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(targetTag))
        {
            AdjustPlayerFireRate(other.gameObject);
            Destroy(gameObject, destroyTime); // Destruir el objeto despu�s del tiempo especificado
        }
    }

    void AdjustPlayerFireRate(GameObject playerObject)
    {
        PlayerShoot playerShoot = playerObject.GetComponent<PlayerShoot>();
        if (playerShoot != null)
        {
            playerShoot.fireRate = 0.5f; // Ajustar el Fire Rate a 0.5
        }
        else
        {
            Debug.LogWarning("El objeto no tiene el componente PlayerShoot.");
        }
    }
}