using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float laneOffset = 3f; // La distancia entre los carriles
    public float jumpForce = 5f;
    public float movementSpeed = 5f;
    private bool isMoving = false;
    private Vector3 targetPosition;

    private int currentLane = 1; // 0: izquierda, 1: centro, 2: derecha

    void Start()
    {
        // Inicializar la posici�n de destino al mismo valor que la posici�n actual del personaje
        targetPosition = transform.position;
    }

    void Update()
    {
        if (!isMoving)
        {
            // Mover el personaje a la izquierda
            if (Input.GetKeyDown(KeyCode.A) && currentLane > 0)
            {
                MoveToLane(currentLane - 1);
            }

            // Mover el personaje a la derecha
            if (Input.GetKeyDown(KeyCode.D) && currentLane < 2)
            {
                MoveToLane(currentLane + 1);
            }

            // Salto del personaje
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }
        }

        // Si deseas que el personaje avance hacia adelante, puedes agregar esta l�nea:
        // transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
    }

    void MoveToLane(int laneIndex)
    {
        currentLane = laneIndex;
        float targetX = (currentLane - 1) * laneOffset;
        targetPosition = new Vector3(targetX, transform.position.y, transform.position.z);
        isMoving = true;
    }

    void Jump()
    {
        // A�adir una fuerza hacia arriba para simular el salto
        GetComponent<Rigidbody>().AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }

    void LateUpdate()
    {
        if (isMoving)
        {
            // Hacer el movimiento hacia el carril objetivo de forma suave usando Lerp
            transform.position = Vector3.Lerp(transform.position, targetPosition, movementSpeed * Time.deltaTime);

            // Comprobar si el personaje est� suficientemente cerca del objetivo para terminar el movimiento
            if (Vector3.Distance(transform.position, targetPosition) < 0.1f)
            {
                transform.position = targetPosition;
                isMoving = false;
            }
        }
    }
}


